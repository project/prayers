------------
Introduction
------------
The Prayers Funeral module displays funeral prayer times. One new content type
is automatically created during installation:

  1) Funeral prayer.

------
Blocks
------
You can optionally enable the pre-defined "Funeral Prayers" block, which will
display upcoming and recent funeral prayers. This block functionality is exposed
by the "Funeral Prayers" view.

Some people's names can be quite long, so you can optionally shorten the date
displayed next to it by visiting the date and time configuration page, clicking
the formats tab and adding a new format, e.g. "j F, g:i a" (1 January, 1:00 pm).
Then click the formats tab and add a new date type and set its format to the
newly added format. Finally, edit the Funeral Prayers view and change the Date
field to use the new date type.

------------------------
Funeral Prayers RSS Feed
------------------------
There is an RSS feed available at www.example.com/funeral/feed, which shows any
upcoming funeral prayers.  This can be picked up by an RSS-to-Email campaign,
such as a MailChimp campaign and automatically emailed to subscribers. You can
optionally subscribe users on your website using the MailChimp module.
