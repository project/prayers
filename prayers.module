<?php

/**
 * @file
 * Provides front-end functionality for Prayers.
 */

/**
 * The date format of prayer times in the database.
 */
define('PRAYERS_DATE_FORMAT', 'm-d');

/**
 * The hour and minutes separator of prayer times in the database.
 */
define('PRAYERS_TIME_SEPARATOR', ':');

/**
 * Implements hook_help().
 */
function prayers_help($path, $arg) {
  switch ($path) {
    case 'admin/help#prayers':
      return '<h3>' . t('About') . '</h3><p>' .
        t('The Prayers module displays prayer start times and congregation times.') . '</p><h3>' .
        t('Usage') . '</h3><dl><dt>' .
        t('Pages') . '</dt><dd>' .
        t('A <em>Prayer Times</em> page is available in the Navigation menu.') . ' ' .
        t('You can configure menus on the <a href="@url">Menus administration page</a>.', array(
          '@url' => url('admin/structure/menu'))) . '</dd><dt>' .
        t('Blocks') . '</dt><dd>' .
        t('A <em>Prayer Times</em> block is available.') . ' ' .
        t('You can configure blocks on the <a href="@url">Blocks administration page</a>.', array(
          '@url' => url('admin/structure/block'))) . '</dd></dl>';
  }
}

/**
 * Implements hook_menu().
 */
function prayers_menu() {

  $items = array();
  $items['prayers/prayer'] = array(
    'title' => 'Prayer Times',
    'page callback' => 'prayers_page',
    'access arguments' => array('access content'),
    'file' => 'includes/prayers.pages.inc',
  );

  $items['admin/config/content/prayers'] = array(
    'title' => 'Prayers',
    'description' => 'Manage prayers settings.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('prayers_admin_settings'),
    'access arguments' => array('administer prayers'),
    'file' => 'includes/prayers.admin.inc',
  );

  $items['prayer/change/%/%'] = array(
    'title' => 'Prayer Congregation Time Changes',
    'page callback' => 'prayers_congregation_rss',
    'page arguments' => array(2, 3),
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK,
  );

  return $items;

}

/**
 * Implements hook_permission().
 */
function prayers_permission() {

  return array(
    'administer prayers' => array(
      'title' => t('Administer prayers'),
      'description' => t('Configure the Prayers module.'),
    ),
  );

}

/**
 * Implements hook_node_info().
 */
function prayers_node_info() {
  return array(
    'prayers_start' => array(
      'name' => t('Prayer start times'),
      'base' => 'prayers',
      'description' => t('Start times for the daily prayers.'),
      'locked' => TRUE,
    ),
    'prayers_congregation' => array(
      'name' => t('Prayer congregation times'),
      'base' => 'prayers',
      'description' => t('Congregation times for the daily prayers.'),
      'locked' => TRUE,
    )
  );
}

/**
 * Implements hook_block_info().
 */
function prayers_block_info() {

  $blocks = array();

  $blocks['prayers_times'] = array(
    'info' => t('Prayer Times'),
  );

  return $blocks;

}

/**
 * Calculates prayer congregation times.
 *
 * @param DateTime $date
 *   The date to get prayer congregation times for.
 *
 * @return array
 *   An associative array of prayer congregation names containing:
 *   - display name: String congregation name.
 *   - start date: String date the prayer congregation time began.
 *   - start time: DateTime start of the prayer time window.
 *   - congregation times: DateTime array of prayer congregation times.
 *   - end time: DateTime end of the prayer time window.
 *   - description: String description of the congregation.
 *   - offset: String minutes to the congregation time after the start time.
 */
function prayers_calc_congregation_times($date) {

  if (!$date) {
    $date = new DateTime();
  }

  $year = $date->format('Y');
  $month = $date->format('m');
  $day = $date->format('d');

  $cong_times = prayers_congregation_times($date);
  $cong_name_list = prayers_names();
  $cong_display_list = array();

  // Get start times as a base for calculating congregation time offsets.
  $start_times = prayers_start_times($date, 1);
  $start_time = NULL;
  $end_time = NULL;

  // Could be multiple congregation times for a prayer, e.g. Ramadhan may
  // require overridden congregation times. Use an associative array to
  // overwrite any instance of the same prayer name within the results.
  // The most restrictive time period will be ultimately used  due to the order
  // in which prayer times are returned within the result set.
  foreach ($cong_times as $cong) {

    $cong_name = $start_time_name = $cong->field_prayer_value;

    if ($cong_name != 'Jumuah' && $cong_name != 'Eid') {
      // Some congregation times are based on a differently named start time.
      if ($cong_name == 'Maghrib') {
        $start_time_name = 'Sunset';
      }
      elseif ($cong_name == 'Asr') {
        $start_time_name = variable_get('prayers_asr_mithl', 'Asr2');
      }

      // Get end time for the prayer, i.e. time at which the next prayer starts.
      $end_time_name = '';

      switch ($start_time_name) {

        case 'Fajr':
          $end_time_name = 'Sunrise';
          break;

        case 'Dhuhr':
          $end_time_name = variable_get('prayers_asr_mithl', 'Asr2');
          break;

        case variable_get('prayers_asr_mithl', 'Asr2'):
          $end_time_name = 'Sunset';
          break;

        case 'Sunset':
          $end_time_name = 'Isha';
          break;

      }

      $start_time_name = drupal_strtolower($start_time_name);
      $end_time_name = drupal_strtolower($end_time_name);

      // Start and end time should be local time.
      $start_time_parts = explode(PRAYERS_TIME_SEPARATOR, $start_times[0]->{'field_' . $start_time_name . '_value'});
      $start_time = new DateTime();
      $start_time->setTimestamp(gmmktime($start_time_parts[0], $start_time_parts[1], 0, $month, $day, $year));

      if ($end_time_name) {
        $end_time_parts = explode(PRAYERS_TIME_SEPARATOR, $start_times[0]->{'field_' . $end_time_name . '_value'});
        $end_time = new DateTime();
        $end_time->setTimestamp(gmmktime($end_time_parts[0], $end_time_parts[1], 0, $month, $day, $year));
      }
      else {
        $end_time = new DateTime();
        $end_time->setTime(23, 59);
      }
    }
    else {
      $start_time = NULL;
    }

    $congs_list = array();
    $cong_time = $cong->field_time_value;

    // Check if congregation time is an offset.
    $first_char = drupal_substr($cong_time, 0, 1);
    if ($first_char == '+') {
      $cong_offset = drupal_substr($cong_time, 1);
      $offset_date = clone $start_time;
      // DateTime->add() would require PHP 5.3+.
      $offset_date->modify("+{$cong_offset} minutes");
      $cong_time = new DateTime();
      $cong_time->setTime($offset_date->format('H'), $offset_date->format('i'));
      $congs_list[] = $cong_time;
    }
    else {
      $cong_offset = NULL;
      // Congregation times can have multiple values.
      $congs = explode(',', $cong_time);
      foreach ($congs as $c) {
        $cong_time_parts = explode(PRAYERS_TIME_SEPARATOR, trim($c));
        // DST-adjust all prayer congregation times except Eid.
        if ($cong_name != 'Eid') {
          $cong_time = new DateTime();
          $cong_time->setTimestamp(gmmktime($cong_time_parts[0], $cong_time_parts[1], 0, $month, $day, $year));
          $congs_list[] = $cong_time;
        }
        else {
          $cong_time = new DateTime();
          $cong_time->setTimestamp(mktime($cong_time_parts[0], $cong_time_parts[1], 0, $month, $day, $year));
          $congs_list[] = $cong_time;
        }
      }
    }

    // Replace prayer name with the configured preference.
    $cong_var_name = drupal_strtolower($cong_name);
    $cong_display_name = filter_xss_admin(variable_get("prayers_strings_{$cong_var_name}", $cong_name));

    $cong_display_list[$cong_name] = array(
      $cong_display_name,
      $cong->field_period_start_value,
      $start_time,
      $congs_list,
      $end_time,
      $cong_offset,
      filter_xss_admin($cong->field_description_value),
    );
  }

  // Sort prayer congregations into chronological order.
  $cong_display_list_sorted = array();

  foreach ($cong_name_list as $cong_name => $cong_display_name) {
    if (isset($cong_display_list[$cong_name])) {
      $cong_display_list_sorted[$cong_name] = $cong_display_list[$cong_name];
    }
  }

  return $cong_display_list_sorted;

}

/**
 * Implements hook_block_view().
 */
function prayers_block_view($name) {

  if ($name == 'prayers_times') {

    $cong_display_list = prayers_calc_congregation_times(NULL);
    $rows = array();

    $now = new DateTime();
    $time_fmt = filter_xss_admin(variable_get('prayers_strings_time_format', 'g:i'));

    foreach ($cong_display_list as $prayer => $value) {
      $display_name = $value[0];
      $start_time = $value[2];
      $cong_times = $value[3];
      $end_time = $value[4];

      // Multiple congregation times may exist, e.g. for Jumuah or Eid.
      foreach ($cong_times as $cong) {

        $start_time_html = '';
        $cong_time_parts = explode(PRAYERS_TIME_SEPARATOR, $cong->format($time_fmt));

        if ($prayer != 'Jumuah' and $prayer != 'Eid') {
          // Vertically line up colons in prayer times.
          $start_time_parts = explode(PRAYERS_TIME_SEPARATOR, $start_time->format($time_fmt));
          $start_time_html = '<div class="prayers-hour">' . $start_time_parts[0] . '</div><div class="prayers-sep">' .
            PRAYERS_TIME_SEPARATOR . '</div><div>' . $start_time_parts[1] . '</div>';
        }

        $row = array(
          'data' => array(
            array('data' => $display_name . ' ' . prayers_cong_desc($display_name, $value[6])),
            $start_time_html,
            '<div class="prayers-hour">' . $cong_time_parts[0] . '</div><div class="prayers-sep">' .
            PRAYERS_TIME_SEPARATOR . '</div><div>' . $cong_time_parts[1] . '</div>',
          ),
          'data-start' => array($start_time != NULL ? $start_time->format('c') : NULL),
          'data-end' => array($start_time != NULL ? $end_time->format('c') : NULL),
        );

        $rows[] = $row;
      }

    }

    // Prayer congregation time descriptions are displayed using jQuery UI.
    drupal_add_library('system', 'ui.dialog');

    $header = array(
      '',
      filter_xss_admin(variable_get('prayers_strings_start', 'Start')),
      filter_xss_admin(variable_get('prayers_strings_congregation', 'Congregation')),
    );

    $content = '<div id="prayers-block-header">' .
      filter_xss_admin(variable_get('prayers_block_header', '')) . '</div>';

    $content .= theme('table', array(
      'header' => $header,
      'rows' => $rows,
      'attributes' => array(
        'id' => array('prayers-block'),
        'class' => array('prayers-data'),
      ),
      'sticky' => FALSE,
      'empty' => t('Currently unavailable.'),
    ));

    $content .= '<div id="prayers-block-footer">' .
      filter_xss_admin(variable_get('prayers_block_footer', '')) . '</div>';

    // Allow use of superscript element.
    $title_date = filter_xss_admin(variable_get('prayers_block_title_date_format', 'l, j<sup>S</sup> F'));
    $title_date = str_replace('sup', '\s\u\p', $title_date);

    $block = array(
      'subject' => t('Prayer Times for !date', array('!date' => $now->format($title_date))),
      'content' => $content,
    );

    return $block;

  }

}

/**
 * Generates HTML for displaying the description of a congregation time.
 *
 * @param string $prayer
 *   The congregation name.
 * @param string $desc
 *   The congregation description.
 *
 * @return string
 *   The HTML for the congregation description.
 */
function prayers_cong_desc($prayer, $desc) {

  $desc_link = filter_xss_admin(variable_get('prayers_block_desc_link', '(?)'));

  return $desc ? l($desc_link, 'javascript:void(0)', array(
    'attributes' => array('onclick' => "prayersDialog('$prayer', '$prayer')"),
    'fragment' => '',
    'external' => TRUE,
  )) . '<div style="display:none"><div id="' . $prayer . '">' . $desc .
    '</div></div>' : '';

}

/**
 * Implements hook_cron().
 */
function prayers_cron() {

  // Update congregation time periods which started or ended on a DST boundary
  // to reflect the coming year's boundaries.
  if (date('W') == 52) {
    $tz = new DateTimeZone(variable_get('date_default_timezone', date_default_timezone_get()));
    $transitions = array();
    $start_of_year_tstamp = strtotime(date('Y-1-1 00:00:00', time()));

    // PHP 5.3+ has begin and end params for timezone_transitions_get() to limit
    // data to this year and next.
    foreach (timezone_transitions_get($tz) as $tran) {
      if ($tran['ts'] > $start_of_year_tstamp) {
        $transitions[] = $tran['ts'];
        if (count($transitions) == 4) {
          break;
        }
      }
    }

    $curr_year_tran1 = new DateTime("@{$transitions[0]}");
    $curr_year_tran2 = new DateTime("@{$transitions[1]}");
    $next_year_tran1 = new DateTime("@{$transitions[2]}");
    $next_year_tran2 = new DateTime("@{$transitions[3]}");

    // Update periods which start on a DST change.
    db_update('field_data_field_period_start')
      ->fields(array('field_period_start_value' => $next_year_tran1->format(PRAYERS_DATE_FORMAT)))
      ->condition('field_period_start_value', $curr_year_tran1->format(PRAYERS_DATE_FORMAT))
      ->execute();

    db_update('field_data_field_period_start')
      ->fields(array('field_period_start_value' => $next_year_tran2->format(PRAYERS_DATE_FORMAT)))
      ->condition('field_period_start_value', $curr_year_tran2->format(PRAYERS_DATE_FORMAT))
      ->execute();

    // Update periods which end the day before a DST change.
    $curr_year_tran1->modify('-1 days');
    $curr_year_tran2->modify('-1 days');
    $next_year_tran1->modify('-1 days');
    $next_year_tran2->modify('-1 days');

    db_update('field_data_field_period_end')
      ->fields(array('field_period_end_value' => $next_year_tran1->format(PRAYERS_DATE_FORMAT)))
      ->condition('field_period_end_value', $curr_year_tran1->format(PRAYERS_DATE_FORMAT))
      ->execute();

    db_update('field_data_field_period_end')
      ->fields(array('field_period_end_value' => $next_year_tran2->format(PRAYERS_DATE_FORMAT)))
      ->condition('field_period_end_value', $curr_year_tran2->format(PRAYERS_DATE_FORMAT))
      ->execute();
  }

}

/**
 * Returns an array of allowed prayer names.
 */
function prayers_names() {

  return array(
    'Fajr' => filter_xss_admin(variable_get('prayers_strings_fajr', 'Fajr')),
    'Dhuhr' => filter_xss_admin(variable_get('prayers_strings_dhuhr', 'Dhuhr')),
    'Asr' => filter_xss_admin(variable_get('prayers_strings_asr', 'Asr')),
    'Maghrib' => filter_xss_admin(variable_get('prayers_strings_maghrib', 'Maghrib')),
    'Isha' => filter_xss_admin(variable_get('prayers_strings_isha', 'Isha')),
    'Jumuah' => filter_xss_admin(variable_get('prayers_strings_jumuah', 'Jumuah')),
    'Eid' => filter_xss_admin(variable_get('prayers_strings_eid', 'Eid')),
  );

}

/**
 * Gets prayer start times from the database.
 *
 * @param DateTime $date
 *   The first date to get prayer start times for.
 * @param string $days
 *   The number of days to get prayer start times for. Defaults to 1.
 *
 * @return array
 *   An array containing:
 *   - title: The date of the prayer start times.
 *   - field_fajr_value: The Fajr prayer start time.
 *   - field_sunrise_value: The Sunrise time.
 *   - field_dhuhr_value: The Dhuhr prayer start time.
 *   - field_asr1_value: The 1st Asr prayer start time.
 *   - field_asr2_value: The 2nd Asr prayer start time.
 *   - field_sunset_value: The Sunset time.
 *   - field_isha_value: The Isha prayer start time.
 */
function prayers_start_times($date, $days = 1) {

  if (!$date) {
    $date = new DateTime();
  }

  // The current date will be day 1 within the result set.
  $days--;

  $query = db_select('node', 'n');
  $query->innerJoin('field_data_field_fajr', 'f', 'f.entity_id = n.nid');
  $query->innerJoin('field_data_field_sunrise', 's1', 's1.entity_id = n.nid');
  $query->innerJoin('field_data_field_dhuhr', 'd', 'd.entity_id = n.nid');
  $query->innerJoin('field_data_field_asr1', 'a1', 'a1.entity_id = n.nid');
  $query->innerJoin('field_data_field_asr2', 'a2', 'a2.entity_id = n.nid');
  $query->innerJoin('field_data_field_sunset', 's2', 's2.entity_id = n.nid');
  $query->innerJoin('field_data_field_isha', 'i', 'i.entity_id = n.nid');

  $query->fields('n', array('title'))
    ->fields('f', array('field_fajr_value'))
    ->fields('s1', array('field_sunrise_value'))
    ->fields('d', array('field_dhuhr_value'))
    ->fields('a1', array('field_asr1_value'))
    ->fields('a2', array('field_asr2_value'))
    ->fields('s2', array('field_sunset_value'))
    ->fields('i', array('field_isha_value'))
    ->condition('n.type', 'prayers_start')
    ->condition('n.status', 1);

  // Save a copy of query so far incase we need to re-use it at year end.
  $union = clone $query;

  // Add sort order incase we need to obtain the union of two queries.
  $query->addExpression(':sort', 'sort', array(':sort' => 1));

  // Exclude leap year data if not required.
  $feb_end = 58;
  if (!$date->format('L') && ($feb_end - $date->format('z')) < $days) {
    $query->condition('n.title', '02-29', '<>');
    // Compensate for the excluded row.
    $days++;
  }

  // Cloning $date doesn't work!
  $end_date = new DateTime();
  if ($date->format('z') + $days < 365) {
    $end_date->setDate($date->format('Y'), $date->format('m'), $date->format('d'));
    $end_date->modify("+{$days} days");
  }
  else {
    $end_date->setDate($date->format('Y'), 12, 31);
  }

  $query->condition('n.title', array($date->format(PRAYERS_DATE_FORMAT), $end_date->format(PRAYERS_DATE_FORMAT)), 'BETWEEN');

  // At end of year take some data from the beginning of following year.
  if (365 - $date->format('z') < $days) {
    // Decrement days because the date will include first day of the year.
    $next_year_days = $days - (365 - $date->format('z')) - 1;
    $next_year_end_date = new DateTime();
    $next_year_end_date->setDate($end_date->format('Y'), 1, 1);
    $next_year_end_date->modify("+{$next_year_days} days");
    $union->condition('n.title', array('01-01', $next_year_end_date->format(PRAYERS_DATE_FORMAT)), 'BETWEEN');
    $union->addExpression(':sort', 'sort', array(':sort' => 2));
    $query->union($union);
  }

  // Drupal 7 contains a bug which does not allow you to order two concatenated
  // result sets: http://drupal.org/node/1145076
  // If you encounter problems, replace $query->union($union); with
  // $query->orderBy('sort', 'asc')->orderBy('n.title', 'asc').
  return $query->execute()
    ->fetchAll();

}

/**
 * Gets prayer congregation times from the database.
 *
 * @param DateTime $date
 *   The date to get prayer congregation times for.
 *
 * @return array
 *   An array containing:
 *   - field_period_start_value: When the period begins.
 *   - field_period_end_value: When the period ends.
 *   - field_period_prayer_value: The prayer name.
 *   - field_period_time_value: The prayer time.
 *   - field_period_description_value: Information about the prayer.
 */
function prayers_congregation_times($date) {

  if (!$date) {
    $date = new DateTime();
  }

  $query = db_select('node', 'n');
  $query->innerJoin('field_data_field_period_start', 's', 's.entity_id = n.nid');
  $query->innerJoin('field_data_field_period_end', 'e', 'e.entity_id = n.nid');
  $query->innerJoin('field_data_field_prayer', 'p', 'p.entity_id = n.nid');
  $query->innerJoin('field_data_field_time', 't', 't.entity_id = n.nid');
  $query->leftJoin('field_data_field_description', 'd', 'd.entity_id = n.nid');
  $query->leftJoin('field_data_field_weight', 'w', 'w.entity_id = n.nid');

  $query->fields('s', array('field_period_start_value'))
    ->fields('e', array('field_period_end_value'))
    ->fields('p', array('field_prayer_value'))
    ->fields('t', array('field_time_value'))
    ->fields('d', array('field_description_value'))
    ->fields('w', array('field_weight_value'))
    ->condition('s.field_period_start_value', $date->format(PRAYERS_DATE_FORMAT), '<=')
    ->condition('e.field_period_end_value', $date->format(PRAYERS_DATE_FORMAT), '>=')
    ->condition('n.type', 'prayers_congregation')
    ->condition('n.status', 1)
    ->orderBy('p.field_prayer_value', 'asc')
    ->orderBy('w.field_weight_value', 'asc');

  return $query->execute()
    ->fetchAll();

}

/**
 * Generates an RSS feed for prayer congregation time changes.
 *
 * @param int $days_offset
 *   The number of days from now for which to obtain congregation time changes.
 * @param string $prayer
 *   The congregation name.
 */
function prayers_congregation_rss($days_offset = 0, $prayer = NULL) {

  global $base_url, $language_content;

  $date = new DateTime();
  $date->modify("+{$days_offset} days");
  $cong_date = $date->format(PRAYERS_DATE_FORMAT);
  $cong_display_list = prayers_calc_congregation_times($date);
  $cong_title = filter_xss_admin(variable_get('prayers_strings_congregation', 'Congregation'));
  $time_fmt = filter_xss_admin(variable_get('prayers_strings_time_format', 'g:i'));
  $now = new DateTime();
  $items = '';

  foreach ($cong_display_list as $cong_name => $cong_values) {

    $cong_display_name = $cong_values[0];
    $period_start = $cong_values[1];
    $cong_times = $cong_values[3];
    $cong_offset = $cong_values[5];

    if ($prayer != 'all' && $cong_name != $prayer && $cong_display_name != $prayer) {
      continue;
    }

    if ($cong_name != 'Eid' && $period_start == $cong_date) {
      $msg = t('@cong will be at @time.', array(
        '@cong' => $cong_display_name,
        '@time' => $cong_times[0]->format($time_fmt),
      ));

      if ($cong_offset != NULL) {
        $msg .= ' ' . t('It will then continue to be prayed @offset minutes after the prayer start time until further notice.',
          array('@offset' => $cong_offset));
      }

      $items .= format_rss_item(
        $cong_display_name,
        url('prayer', array('absolute' => TRUE)),
        $msg,
        array(
          array(
            'key' => 'guid',
            'value' => $date->format('Y-') . $cong_date . '-' . $cong_display_name,
            'attributes' => array(
              'isPermaLink' => 'false',
            ),
          ),
          array(
            'key' => 'pubDate',
            'value' => $now->format(DateTime::RSS),
          ),
        )
      );
    }
  }

  $channel = array(
    'title' => t('@mosque @cong_title Times', array(
      '@mosque' => variable_get('site_name', 'Mosque'),
      '@cong_title' => $cong_title,
    )),
    'description' => t('Please note the change in prayer congregation times on @date.', array(
      '@date' => $date->format('l, jS F Y'),
    )),
  );

  $namespaces = array('xmlns:dc' => 'http://purl.org/dc/elements/1.1/');

  $channel_defaults = array(
    'version' => '2.0',
    'title' => variable_get('site_name', 'Mosque'),
    'link' => $base_url,
    'description' => variable_get('feed_description', ''),
    'language' => $language_content->language,
  );

  $channel_extras = array_diff_key($channel, $channel_defaults);
  $channel = array_merge($channel_defaults, $channel);

  $output = "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n";
  $output .= "<rss version=\"" . $channel["version"] . "\" xml:base=\"" . $base_url . "\" " . drupal_attributes($namespaces) . ">\n";
  $output .= format_rss_channel($channel['title'], $channel['link'], $channel['description'], $items, $channel['language'], $channel_extras);
  $output .= "</rss>\n";

  drupal_add_http_header('Content-Type', 'application/rss+xml; charset=utf-8');
  print $output;

}

/**
 * Implements hook_ctools_plugin_api().
 */
function prayers_ctools_plugin_api($module = '', $api = '') {

  if ($module == 'feeds' && $api == 'feeds_importer_default') {
    return array(
      'version' => 1,
    );
  }

}

/**
 * Implements hook_ctools_plugin_directory().
 */
function prayers_ctools_plugin_directory($module, $plugin) {

  if ($module == 'feeds_tamper') {
    return 'plugins/' . $module;
  }

}

/**
 * Implements hook_feeds_importer_default().
 */
function prayers_feeds_importer_default() {

  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'prayers_start_times';
  $feeds_importer->config = array(
    'name' => 'Prayer Start Times',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => FALSE,
        'directory' => 'public://feeds',
        'allowed_schemes' => array(
          0 => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'mappings' => array(
          0 => array(
            'source' => 'Date',
            'target' => 'title',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'Fajr',
            'target' => 'field_fajr',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'Sunrise',
            'target' => 'field_sunrise',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'Dhuhr',
            'target' => 'field_dhuhr',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'Asr1',
            'target' => 'field_asr1',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'Asr2',
            'target' => 'field_asr2',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'Sunset',
            'target' => 'field_sunset',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'Isha',
            'target' => 'field_isha',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '1',
        'input_format' => 'plain_text',
        'authorize' => 1,
        'skip_hash_check' => 0,
        'bundle' => 'prayers_start',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );

  $export['prayers_start_times'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'prayers_congregation_times';
  $feeds_importer->config = array(
    'name' => 'Prayer Congregation Times',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'allowed_extensions' => 'txt csv tsv xml opml',
        'direct' => FALSE,
        'directory' => 'public://feeds',
        'allowed_schemes' => array(
          0 => 'public',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'expire' => '-1',
        'author' => '1',
        'mappings' => array(
          0 => array(
            'source' => 'Title',
            'target' => 'title',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'Start',
            'target' => 'field_period_start',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'End',
            'target' => 'field_period_end',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'Prayer',
            'target' => 'field_prayer',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'Time',
            'target' => 'field_time',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'Description',
            'target' => 'field_description',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '1',
        'input_format' => 'plain_text',
        'authorize' => 1,
        'skip_hash_check' => 0,
        'bundle' => 'prayers_congregation',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );

  $export['prayers_congregation_times'] = $feeds_importer;

  return $export;

}
