------------
Introduction
------------
The Prayers module displays prayer start times and prayer congregation times.
Two new content types are automatically created during installation:

  1) Prayer start times.
  2) Prayer congregation times.

-----
Pages
-----
A "Prayer Times" page will be available at www.example.com/prayers/prayer. You
can create an alias for this path in order to shorten it.

------
Blocks
------
You can optionally enable the pre-defined "Prayer Times" block, which will
display prayer start times and prayer congregation times for the current day.
The current prayer will be highlighted, except for the period between midnight
and the Fajr prayer.

--------------
Administration
--------------
You can configure settings via the "Prayer" administration page. You can set:

  1) Display strings such as prayer names and table headings.
  2) Formats to use when displaying dates.
  3) Which asr prayer start time(s) to show and which of them is used as a basis
     for the asr prayer congregation time.

----------------
Optional Modules
----------------

  --------
  1. Feeds
  --------
  Feeds + Feeds Admin UI submodule (feeds-7.x-2.0-alpha8 is known to work).

  By installing the optional Feeds module, you can import prayer start times and
  prayer congregation times from a file for the whole year. Otherwise you
  will have to add a new content item for each day's prayer start times and for
  each period's congregation times, one-by-one. This module contains pre-defined
  Feeds module importers for prayer start times and prayer congregation times.

  ----------------------------------------------------------------------
  Importing prayer start times and prayer congregation times from a file
  ----------------------------------------------------------------------
  Visit www.example.com/import
    -> Prayer Start Times
         File (choose a file)
         Click Import

  Note:
  1) Prayer start times and prayer congregation times must be uploaded in
     24-hour UTC format. The exception is Eid congregation times, which must be
     a local time. This is because you may choose to display the Eid prayer
     times a week before  Eid, during which a DST time change may occur. The
     configuration page allows display of prayer start times in 12-hour format.
  2) The prayer start times file must include a column for both Asr1 and Asr2,
     even if one of them is unused. Example CSV and TSV files are included.
  3) If you are using a CSV file containing commas within the values, e.g. you
     have multiple comma-separated Eid prayer congregation times, then you must
     enclose the value within quotes. If you experience problems, try fixing it
     in a text editor or using a different delimiter, e.g. tabs.

  ---------------
  2. Feeds Tamper
  ---------------
  Modules: Feeds Tamper + Feeds Tamper Admin UI

  By installing the optional Feeds Tamper module, you can assign validation
  rules so that the data that you upload via a file will actually work. This
  module contains the Feeds Tamper plugins, but they will have to be manually
  assigned to each of the two importers as follows:

  In Structure->Feeds Importers, add the following tamper plugins:

  1) Prayer Start Times:
     Title = Prayers date validation
     Fajr, Sunrise, Dhuhr, Asr1, Asr2, Sunset and Isha = Prayers time validation

  2) Prayer Congregation Times:
     Title = Rewrite (enter any pattern, e.g.: [prayer] [start] to [end])
     Prayer = Prayers name validation
     Start and End = Prayers date validation
     Time = Prayers time validation (Allow offsets = Yes)

------------
Prayer Times
------------
The following prayer congregation times can be created:

  1) Fajr
  2) Dhuhr
  3) Asr
  4) Maghrib
  5) Isha
  6) Jumuah
  7) Eid

Note:
  1) If you have multiple congregation times on the same day for a particular
     prayer, e.g. Jumuah or Eid, create one node only and separate each
     congregation time with a comma, e.g. 06:00, 06:30, 07:00.
  2) Prayer congregation times can be an offset in minutes from the prayer start
     time, e.g. +10.
  3) You can enter prayer congregation times which overlap other periods of the
     year, e.g. the Fajr prayer congregation times during Ramadhan may differ
     from the usual prayer congregation times for that time of the year. The
     period with the highest weight will be used.
  4) For one-off prayer congregation times (Eid), set the period to begin on
     the date that you wish to start displaying it and ending on the date of
     the Eid prayer.

---
CSS
---
The "Prayer Times" page at www.example.com/prayers/prayer displays prayer start
times for desktops and mobile devices. The mobile version is hidden by default.
If you have a responsive theme, then you must display either the desktop or
mobile version as appropriate, e.g.:

wide.css:

  #prayers-times-mobile {
    display: none;
  }

  #prayers-times {
    display: table;
  }

mobile.css:

  #prayers-times-mobile {
    display: table;
  }

  #prayers-times {
    display: none;
  }

----------------------------------
Prayer Congregation Times RSS Feed
----------------------------------
An RSS feed is available at www.example.com/prayer/change/%d/%s which shows
whether the congregation time for prayer %s (e.g. Isha or 'all') will change
%d days in the future. Set %d to zero for the current day.  This can be picked
up by an RSS-to-Email campaign, such as a MailChimp campaign and automatically
emailed to interested subscribers. You can optionally subscribe users on your
website using the MailChimp module.

----------------------------------------------
Prayer Congregation Times Daylight Saving Cron
----------------------------------------------
At the end of each calendar year, prayer congregation times which start or end
on a DST boundary are changed automatically to the new DST boundary dates for
the coming year. 

Note:
This will only work if you run one of the following:
  1) Configuration->System->Cron
  2) A crontab entry which runs scripts/cron-curl.sh or equivalent.

------------------------------------------------
Prayer Congregation Times Automatic Unpublishing
------------------------------------------------
Optionally install the Scheduler module (and Date Popup module for a date
calendar) to unpublish prayer congregation periods which have passed so that
they are not displayed the following year accidentally, e.g. Eid or Ramadhan.

Note:
This will only work if you run one of the following:
  1) Configuration->System->Cron
  2) A crontab entry which runs scripts/cron-curl.sh or equivalent.

------
Images
------
The cross image used in the Prayer Times block when displaying prayers which
do not have a start time, i.e. Jumuah and Eid, was copied from:

http://openclipart.org/detail/1645/cancel-by-dagobert83
