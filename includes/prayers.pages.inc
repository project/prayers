<?php

/**
 * @file
 * User page callbacks for the Prayers module.
 */

/**
 * Menu callback; prints a listing of prayer start times.
 *
 * @return array
 *   The render array for the page.
 */
function prayers_page() {

  $start_times = prayers_start_times(NULL, variable_get('prayers_times_days', 7));

  date_default_timezone_set(variable_get('date_default_timezone', date_default_timezone_get()));

  $date_fmt = filter_xss_admin(variable_get('prayers_strings_date_format', 'M d'));
  $day_fmt  = filter_xss_admin(variable_get('prayers_strings_day_format', 'D'));
  $time_fmt = filter_xss_admin(variable_get('prayers_strings_time_format', 'g:i'));
  $year     = date('Y');
  $rows     = array();

  foreach ($start_times as $node) {

    $timestamp = strtotime(date('Y-') . $node->title);
    list($month, $day) = explode('-', $node->title);

    // Vertically line up colons in prayer times.
    $fajr_time_parts = explode(':', date($time_fmt, gmmktime(drupal_substr($node->field_fajr_value, 0, 2),
      drupal_substr($node->field_fajr_value, 3, 2), 0, $month, $day, $year)));
    $sunrise_time_parts = explode(':', date($time_fmt, gmmktime(drupal_substr($node->field_sunrise_value, 0, 2),
      drupal_substr($node->field_sunrise_value, 3, 2), 0, $month, $day, $year)));
    $dhuhr_time_parts = explode(':', date($time_fmt, gmmktime(drupal_substr($node->field_dhuhr_value, 0, 2),
      drupal_substr($node->field_dhuhr_value, 3, 2), 0, $month, $day, $year)));
    $asr1_time_parts = explode(':', date($time_fmt, gmmktime(drupal_substr($node->field_asr1_value, 0, 2),
      drupal_substr($node->field_asr1_value, 3, 2), 0, $month, $day, $year)));
    $asr2_time_parts = explode(':', date($time_fmt, gmmktime(drupal_substr($node->field_asr2_value, 0, 2),
      drupal_substr($node->field_asr2_value, 3, 2), 0, $month, $day, $year)));
    $sunset_time_parts = explode(':', date($time_fmt, gmmktime(drupal_substr($node->field_sunset_value, 0, 2),
      drupal_substr($node->field_sunset_value, 3, 2), 0, $month, $day, $year)));
    $isha_time_parts = explode(':', date($time_fmt, gmmktime(drupal_substr($node->field_isha_value, 0, 2),
      drupal_substr($node->field_isha_value, 3, 2), 0, $month, $day, $year)));

    $row = array(
      'date' => date($date_fmt, $timestamp),
      'day' => date($day_fmt, $timestamp),
      'fajr' => prayers_make_time($fajr_time_parts[0], $fajr_time_parts[1]),
      'sunrise' => prayers_make_time($sunrise_time_parts[0], $sunrise_time_parts[1]),
      'dhuhr' => prayers_make_time($dhuhr_time_parts[0], $dhuhr_time_parts[1]),
    );

    if (variable_get('prayers_asr1_display', TRUE) == TRUE) {
      $row['asr1'] = prayers_make_time($asr1_time_parts[0], $asr1_time_parts[1]);
    }

    if (variable_get('prayers_asr2_display', TRUE) == TRUE) {
      $row['asr2'] = prayers_make_time($asr2_time_parts[0], $asr2_time_parts[1]);
    }

    $row['sunset'] = prayers_make_time($sunset_time_parts[0], $sunset_time_parts[1]);
    $row['isha'] = prayers_make_time($isha_time_parts[0], $isha_time_parts[1]);

    $rows[] = $row;
  }

  $page['header'] = array(
    '#type' => 'markup',
    '#markup' => '<div id="prayers-times-header">' .
    filter_xss_admin(variable_get('prayers_times_header', '')) . '</div>',
  );

  $header = array('',
    filter_xss_admin(variable_get('prayers_strings_day', 'Day')),
    filter_xss_admin(variable_get('prayers_strings_fajr', 'Fajr')),
    filter_xss_admin(variable_get('prayers_strings_sunrise', 'Sunrise')),
    filter_xss_admin(variable_get('prayers_strings_dhuhr', 'Dhuhr')),
  );

  if (variable_get('prayers_asr1_display', TRUE) == TRUE) {
    $header[] = filter_xss_admin(variable_get('prayers_strings_asr1', 'Asr 1<sup>st</sup>'));
  }

  if (variable_get('prayers_asr2_display', TRUE) == TRUE) {
    $header[] = filter_xss_admin(variable_get('prayers_strings_asr2', 'Asr 2<sup>nd</sup>'));
  }

  $header[] = filter_xss_admin(variable_get('prayers_strings_sunset', 'Sunset'));
  $header[] = filter_xss_admin(variable_get('prayers_strings_isha', 'Isha'));

  $page['start times'] = array(
    '#theme' => 'table',
    '#attributes' => array('id' => array('prayers-times'), 'class' => array('prayers-data')),
    '#sticky' => FALSE,
    '#empty' => t('Prayer times are currently unavailable.'),
    '#rows' => $rows,
    '#header' => $header,
  );

  // Mobile version for smaller screens.
  $days = variable_get('prayers_times_days_mobile', 3);
  $header_mobile = array('');
  $rows_mobile = array();

  // Transpose wide display column headings vertically into first column.
  if ($rows && $rows[0]) {
    $i = 0;
    foreach ($rows[0] as $key => $value) {
      if ($key != 'date') {
        $rows_mobile[$key][] = $header[$i];
      }
      $i++;
    }
  }

  for ($i = 0; $i < $days; $i++) {
    if ($rows && $rows[$i]) {
      foreach ($rows[$i] as $key => $value) {
        if ($key == 'date') {
          $header_mobile[] = $value;
        }
        else {
          $rows_mobile[$key][] = $value;
        }
      }
    }
  }

  $page['start times mobile'] = array(
    '#theme' => 'table',
    '#attributes' => array('id' => array('prayers-times-mobile'), 'class' => array('prayers-data')),
    '#sticky' => FALSE,
    '#empty' => t('Prayer times unavailable.'),
    '#rows' => $rows_mobile,
    '#header' => $header_mobile,
  );

  // Javascript variable for automatic scrolling.
  if (variable_get('prayers_scroll')) {
    drupal_add_js(array('prayers' => array('scroll' => filter_xss_admin(variable_get('prayers_scroll')))), 'setting');
  }

  $page['footer'] = array(
    '#type' => 'markup',
    '#markup' => '<div id="prayers-times-footer">' .
    filter_xss_admin(variable_get('prayers_times_footer', '')) . '</div>',
  );

  return $page;
}

/**
 * Create HTML for displaying a time.
 *
 * @param string $hour
 *   Required. The hour portion of the time.
 * @param string $min
 *   Required. The minute portion of the time.
 *
 * @return string
 *   The HTML for the time.
 */
function prayers_make_time($hour, $min) {
  return '<div class="prayers-hour">' . $hour . '</div><div class="prayers-sep">:</div><div>' . $min . '</div>';
}
