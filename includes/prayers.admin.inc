<?php

/**
 * @file
 * Prayers configuration settings.
 */

/**
 * Return the global settings form.
 */
function prayers_admin_settings() {

  $form = array();

  $form['prayers_strings'] = array(
    '#type' => 'fieldset',
    '#title' => t('Display settings'),
    '#weight' => 1,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['prayers_strings']['prayers_strings_day'] = array(
    '#type' => 'textfield',
    '#title' => t('Day heading'),
    '#default_value' => variable_get('prayers_strings_day', 'Day'),
    '#description' => t('The heading displayed for the day. E.g. Day.'),
  );

  $form['prayers_strings']['prayers_strings_fajr'] = array(
    '#type' => 'textfield',
    '#title' => t('Fajr heading'),
    '#default_value' => variable_get('prayers_strings_fajr', 'Fajr'),
    '#description' => t('The heading displayed for the fajr prayer. E.g. Fajr.'),
  );

  $form['prayers_strings']['prayers_strings_sunrise'] = array(
    '#type' => 'textfield',
    '#title' => t('Sunrise heading'),
    '#default_value' => variable_get('prayers_strings_sunrise', 'Sunrise'),
    '#description' => t('The heading displayed for sunrise. E.g. Sunrise.'),
  );

  $form['prayers_strings']['prayers_strings_dhuhr'] = array(
    '#type' => 'textfield',
    '#title' => t('Dhuhr heading'),
    '#default_value' => variable_get('prayers_strings_dhuhr', 'Dhuhr'),
    '#description' => t('The heading displayed for the dhuhr prayer. E.g. Dhuhr.'),
  );

  $form['prayers_strings']['prayers_strings_asr'] = array(
    '#type' => 'textfield',
    '#title' => t('Asr heading'),
    '#default_value' => variable_get('prayers_strings_asr', 'Asr'),
    '#description' => t('The heading displayed for the asr prayer. E.g. Asr.'),
  );

  $form['prayers_strings']['prayers_strings_asr1'] = array(
    '#type' => 'textfield',
    '#title' => t('Asr 1 heading'),
    '#default_value' => variable_get('prayers_strings_asr1', 'Asr 1<sup>st</sup>'),
    '#description' => t('The heading displayed for the 1st asr mithl prayer. E.g. Asr 1&lt;sup&gt;st&lt;/sup&gt;.'),
  );

  $form['prayers_strings']['prayers_strings_asr2'] = array(
    '#type' => 'textfield',
    '#title' => t('Asr 2 heading'),
    '#default_value' => variable_get('prayers_strings_asr2', 'Asr 2<sup>nd</sup>'),
    '#description' => t('The heading displayed for the 2nd asr mithl prayer. E.g. Asr 2&lt;sup&gt;nd&lt;/sup&gt;.'),
  );

  $form['prayers_strings']['prayers_strings_sunset'] = array(
    '#type' => 'textfield',
    '#title' => t('Sunset heading'),
    '#default_value' => variable_get('prayers_strings_sunset', 'Sunset'),
    '#description' => t('The heading displayed for sunset. E.g. Sunset.'),
  );

  $form['prayers_strings']['prayers_strings_maghrib'] = array(
    '#type' => 'textfield',
    '#title' => t('Maghrib heading'),
    '#default_value' => variable_get('prayers_strings_maghrib', 'Maghrib'),
    '#description' => t('The heading displayed for the maghrib prayer. E.g. Maghrib.'),
  );

  $form['prayers_strings']['prayers_strings_isha'] = array(
    '#type' => 'textfield',
    '#title' => t('Isha heading'),
    '#default_value' => variable_get('prayers_strings_isha', 'Isha'),
    '#description' => t('The heading displayed for the isha prayer. E.g. Isha.'),
  );

  $form['prayers_strings']['prayers_strings_jumuah'] = array(
    '#type' => 'textfield',
    '#title' => t('Jumuah heading'),
    '#default_value' => variable_get('prayers_strings_jumuah', 'Jumuah'),
    '#description' => t('The heading displayed for the jumuah prayer. E.g. Jumuah.'),
  );

  $form['prayers_strings']['prayers_strings_eid'] = array(
    '#type' => 'textfield',
    '#title' => t('Eid heading'),
    '#default_value' => variable_get('prayers_strings_eid', 'Eid'),
    '#description' => t('The heading displayed for the eid prayer. E.g. Eid.'),
  );

  $form['prayers_strings']['prayers_strings_start'] = array(
    '#type' => 'textfield',
    '#title' => t('Start heading'),
    '#default_value' => variable_get('prayers_strings_start', 'Start'),
    '#description' => t('The heading displayed for prayer start times. E.g. Start.'),
  );

  $form['prayers_strings']['prayers_strings_congregation'] = array(
    '#type' => 'textfield',
    '#title' => t('Congregation heading'),
    '#default_value' => variable_get('prayers_strings_congregation', 'Congregation'),
    '#description' => t('The heading displayed for prayer congregation times. E.g. Congregation.'),
  );

  $form['prayers_strings']['prayers_strings_date_format'] = array(
    '#type' => 'textfield',
    '#title' => t('Date format'),
    '#default_value' => variable_get('prayers_strings_date_format', 'M d'),
    '#description' => t('The <a href="http://uk.php.net/manual/en/datetime.formats.date.php">date format</a> for prayers. E.g. M d.'),
  );

  $form['prayers_strings']['prayers_strings_day_format'] = array(
    '#type' => 'textfield',
    '#title' => t('Day format'),
    '#default_value' => variable_get('prayers_strings_day_format', 'D'),
    '#description' => t('The <a href="http://uk.php.net/manual/en/datetime.formats.date.php">day format</a> for prayers. E.g. D.'),
  );

  $form['prayers_strings']['prayers_strings_time_format'] = array(
    '#type' => 'textfield',
    '#title' => t('Time format'),
    '#default_value' => variable_get('prayers_strings_time_format', 'g:i'),
    '#description' => t('The <a href="http://uk.php.net/manual/en/datetime.formats.time.php">time format</a> for prayers. E.g. g:i for 12-hour or H:i for 24-hour.'),
  );

  $form['prayers_asr'] = array(
    '#type' => 'fieldset',
    '#title' => t('Asr settings'),
    '#weight' => 2,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['prayers_asr']['prayers_asr1_display'] = array(
    '#type' => 'checkbox',
    '#title' => t('1st asr mithl'),
    '#default_value' => variable_get('prayers_asr1_display', TRUE),
    "#description" => t('Display the 1st asr mithl in the prayer start times.'),
  );

  $form['prayers_asr']['prayers_asr2_display'] = array(
    '#type' => 'checkbox',
    '#title' => t('2nd asr mithl'),
    '#default_value' => variable_get('prayers_asr2_display', TRUE),
    "#description" => t('Display the 2nd asr mithl in the prayer start times.'),
  );

  $form['prayers_asr']['prayers_asr_mithl'] = array(
    '#type' => 'radios',
    '#title' => t('Asr mithl followed'),
    '#default_value' => variable_get('prayers_asr_mithl', 'Asr2'),
    '#options' => array('Asr1' => t('1st asr mithl'), 'Asr2' => t('2nd asr mithl')),
    '#description' => t('The mithl that the asr congregation time is based upon.'),
  );

  $form['prayers_times'] = array(
    '#type' => 'fieldset',
    '#title' => t('Start times settings'),
    '#weight' => 3,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['prayers_times']['prayers_times_days'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of days to show on a large device'),
    '#default_value' => variable_get('prayers_times_days', 7),
    '#description' => t('The number of days to show prayer times for. E.g. 7.'),
  );

  $form['prayers_times']['prayers_times_days_mobile'] = array(
    '#type' => 'textfield',
    '#title' => t('Number of days to show on a small device'),
    '#default_value' => variable_get('prayers_times_days_mobile', 3),
    '#description' => t('The number of days to show prayer times for. E.g. 3.'),
  );

  $form['prayers_times']['prayers_times_header'] = array(
    '#type' => 'textarea',
    '#rows' => 3,
    '#columns' => 40,
    '#title' => t('Header text'),
    '#default_value' => variable_get('prayers_times_header'),
    '#description' => t('The text displayed above the prayer start times.'),
  );

  $form['prayers_times']['prayers_times_footer'] = array(
    '#type' => 'textarea',
    '#rows' => 3,
    '#columns' => 40,
    '#title' => t('Footer text'),
    '#default_value' => variable_get('prayers_times_footer'),
    '#description' => t('The text displayed below the prayer start times.'),
  );

  $form['prayers_block'] = array(
    '#type' => 'fieldset',
    '#title' => t('Block settings'),
    '#weight' => 4,
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $form['prayers_block']['prayers_block_title_date_format'] = array(
    '#type' => 'textfield',
    '#title' => t('Title date format'),
    '#default_value' => variable_get('prayers_block_title_date_format', 'j<sup>S</sup> F'),
    '#description' => t('The <a href="http://uk.php.net/manual/en/datetime.formats.date.php">date format</a> for the current date. E.g. j&lt;sup&gt;S&lt;/sup&gt; F.'),
  );

  $form['prayers_block']['prayers_block_desc_link'] = array(
    '#type' => 'textfield',
    '#title' => t('Description link'),
    '#default_value' => variable_get('prayers_block_desc_link', '(?)'),
    '#description' => t('The text to display as a link for more information about the congregation time. E.g. (?).'),
  );

  $form['prayers_block']['prayers_block_header'] = array(
    '#type' => 'textarea',
    '#rows' => 3,
    '#columns' => 40,
    '#title' => t('Header text'),
    '#default_value' => variable_get('prayers_block_header'),
    '#description' => t('The text displayed above the prayer block.'),
  );

  $form['prayers_block']['prayers_block_footer'] = array(
    '#type' => 'textarea',
    '#rows' => 3,
    '#columns' => 40,
    '#title' => t('Footer text'),
    '#default_value' => variable_get('prayers_block_footer'),
    '#description' => t('The text displayed below the prayer block.'),
  );

  $form['prayers_scroll'] = array(
    '#type' => 'textfield',
    '#title' => t('Scroll position'),
    '#default_value' => variable_get('prayers_scroll'),
    '#description' => t('The HTML element or number of pixels to scroll to when loading the prayer times page on a mobile device. E.g. #banner or 200.'),
  );

  return system_settings_form($form);
}
