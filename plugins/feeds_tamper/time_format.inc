<?php

/**
 * @file
 * Checks whether a time is in a valid format.
 */

$plugin = array(
  'form' => 'prayers_time_format_form',
  'callback' => 'prayers_time_format_callback',
  'name' => 'Prayers time validation',
  'multi' => 'direct',
  'category' => 'Text',
);

/**
 * Configuration page customization.
 */
function prayers_time_format_form($importer, $element_key, $settings) {

  $form = array();
  $form['help']['#markup'] = t('Checks that the prayer time is in the hh:mm format.');
  $form['offset'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow offsets'),
    '#default_value' => isset($settings['offset']) ? $settings['offset'] : FALSE,
    '#description' => t('Additionally allow a format of +N (in minutes).'),
  );

  return $form;

}

/**
 * Validation of the submitted value.
 */
function prayers_time_format_callback($result, $item_key, $element_key, &$field, $settings) {

  // 24-hour format.
  $regexp = '^([0-1]\d|2[0-3]):[0-5]\d$';

  if ($settings['offset']) {
    $regexp .= '|\+\d+';
  }

  // Could be multiple time values.
  $times = explode(',', $field);
  foreach ($times as $time) {
    if (!preg_match('/' . $regexp . '/', trim($time))) {
      form_set_error('feeds_tamper', t('@field must be hh:mm.', array('@field' => $field)));
      unset($result->items[$item_key]);
    }
  }

}
