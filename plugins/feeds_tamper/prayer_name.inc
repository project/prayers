<?php

/**
 * @file
 * Checks whether a prayer name is an allowed value.
 */

$plugin = array(
  'form' => 'prayers_prayer_name_form',
  'callback' => 'prayers_prayer_name_callback',
  'name' => 'Prayers name validation',
  'multi' => 'direct',
  'category' => 'Text',
);

/**
 * Configuration page customization.
 */
function prayers_prayer_name_form($importer, $element_key, $settings) {

  $form = array();
  $form['help']['#markup'] = t('Checks that the prayer name is an allowed value.');

  return $form;

}

/**
 * Validation of the submitted value.
 */
function prayers_prayer_name_callback($result, $item_key, $element_key, &$field, $settings) {

  $prayers = prayers_names();
  $match = FALSE;

  foreach ($prayers as $prayer_name => $prayer_display_name) {
    if ($field == $prayer_name || $field == $prayer_display_name) {
      $match = TRUE;

      // If it is the friendly name then change it to the system name.
      if ($field == $prayer_display_name) {
        $field = $prayer_name;
      }

      break;
    }
  }

  if ($match == FALSE) {
    form_set_error('feeds_tamper', t('@field must be one of: @prayers.', array(
      '@field' => $field,
      '@prayers' => implode(', ', $prayers),
    )));
    unset($result->items[$item_key]);
  }

}
