<?php

/**
 * @file
 * Checks whether a date is in a valid format.
 */

$plugin = array(
  'form' => 'prayers_date_format_form',
  'callback' => 'prayers_date_format_callback',
  'name' => 'Prayers date validation',
  'multi' => 'direct',
  'category' => 'Text',
);

/**
 * Configuration page customization.
 */
function prayers_date_format_form($importer, $element_key, $settings) {

  $form = array();
  $form['help']['#markup'] = t('Checks that the prayer date is in the MM-DD format.');

  return $form;

}

/**
 * Validation of the submitted value.
 */
function prayers_date_format_callback($result, $item_key, $element_key, &$field, $settings) {

  $regexp = '(0[1-9]|1[0-2])\-(0[1-9]|[1-2]\d|3[0-1])';

  if (!preg_match('/' . $regexp . '/', $field)) {
    form_set_error('feeds_tamper', t('@field must be MM-DD.', array('@field' => $field)));
    unset($result->items[$item_key]);
  }

}
