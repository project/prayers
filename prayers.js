/**
 * @file
 * Client-side JavaScript for Prayers.
 */

/**
 * Pop-up dialog for information messages.
 */
function prayersDialog(elem, title) {
  jQuery('#' + elem).dialog({ title: title,
                              modal: true,
                              dialogClass: 'prayers-dialog',
                              resizable: false
  });
}

/**
 * Page onload functionality.
 */
Drupal.behaviors.prayers = {
  attach: function(context, settings) {
    // Auto-scroll prayer start times page on mobiles
    if (jQuery('#prayers-times-mobile').is(':visible')) {
      if (settings.prayers && settings.prayers.scroll) {
        var scroll = settings.prayers.scroll;
        if (isNaN(parseInt(scroll))) {
          scroll = jQuery(scroll).offset().top;
        }
      }
      jQuery('html, body').animate({scrollTop: scroll}, 1000);
    }

    // Highlight current prayer within prayer times block
    if (jQuery('#prayers-block').is(':visible')) {
      var now = new Date();
      now.setTime(now.getTime() - (now.getTimezoneOffset() * 60000));
      var nowISO = now.toISOString().substring(0, 19);

      jQuery('#prayers-block tr').filter(function() {
        return jQuery(this).data('start') && jQuery(this).data('start').substring(0, 19) <= nowISO &&
          jQuery(this).data('end').substring(0, 19) > nowISO;
      }).attr('id', 'prayers-current');
    }
  }
};
